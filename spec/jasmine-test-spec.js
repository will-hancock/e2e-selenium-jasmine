/* Automate e2e testing with selenium, webdriverJS and Jasmine
 * - Will Hancock
 */

"use strict";

// Dependancies
var webdriver				= require('selenium-webdriver'),
	SeleniumServer			= require('selenium-webdriver/remote').SeleniumServer,

	config					= require('../config/config.json');


// CONSTANTS
var TEST_TIMEOUT			= config.testTimeout,
	BROWSER					= config.browser.name,

	PATH_TO_SELENIUM_JAR	= config.selenium.local.pathToJar,
	SELENIUM_PORT			= config.selenium.local.port,

	FLAGS					= process.argv.splice(2);


// Create standalone selenium server - so we can use Firefox etc.
var server = new SeleniumServer(PATH_TO_SELENIUM_JAR, {
	port: SELENIUM_PORT
});
server.start();


// Configure WebdriverJS
var driver = new webdriver.Builder().
	usingServer(server.address()/*'http://vmq-qa-au1'*/).
    withCapabilities(webdriver.Capabilities[BROWSER]()).
    build();

// print process.argv
//FLAGS.forEach(function (val, index, array) {
//	console.log(index + ': ' + val);
//});


// Begin tests

describe('basic test', function () {

	var searchResults = [];

	it('should search for webdriver and land on results page', function () {
		var match = 'webdriver - Google Search',
			title = '';

		driver.get("http://www.google.com");
		driver.findElement(webdriver.By.name("q")).sendKeys("webdriver");
		driver.findElement(webdriver.By.name("btnG")).click();

		// wait for page title, we know we are there
		waitsFor(function () {
			driver.getTitle().then(function (_title) {
				title = _title;
			});
			return title === match;
		}, 'Test page title, so we know page is loaded', TEST_TIMEOUT);

		// test title is correct
		runs(function () {
			expect(title).toEqual(match);
		});
	});




	it('should return 10 results', function () {	

		// wait for page title, we know we are there
		waitsFor(function () {
			driver.findElements(webdriver.By.className("g")).then(function (elements_arr) {
				searchResults = elements_arr;
			});
			return searchResults.length > 0;
		}, 'Getting search results', TEST_TIMEOUT);

		// test title is correct
		runs(function () {
			expect(searchResults.length).toBeGreaterThan(0);
			expect(searchResults.length).toBe(10);
		});
	});



	
	it('should go to the first result', function () {
		expect(searchResults.length).toBe(10);
	});

});