﻿/* Automate e2e testing with selenium, webdriverJS and Jasmine
 * - Will Hancock
 */

// Dependancies
var webdriver				= require('selenium-webdriver'),
	SeleniumServer			= require('selenium-webdriver/remote').SeleniumServer,

	config					= require('../config/config.json');


// CONSTANTS
var TEST_TIMEOUT			= config.testTimeout,
	BROWSER					= config.browser.name,

	PATH_TO_SELENIUM_JAR	= config.selenium.standalone.pathToJar,
	SELENIUM_PORT			= config.selenium.standalone.port;


// Create standalone selenium server - so we can use Firefox etc.
var server = new SeleniumServer(PATH_TO_SELENIUM_JAR, {
	port: SELENIUM_PORT
});
server.start();


// Configure WebdriverJS
var driver = new webdriver.Builder().
	usingServer(server.address()).
    withCapabilities(webdriver.Capabilities[BROWSER]()).
    build();

