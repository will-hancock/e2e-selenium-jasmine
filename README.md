﻿
# Automate e2e testing with selenium, webdriverJS and Jasmine 

### Technical contacts
* Will Hancock - Web Developer - [will.hancock007@gmail.com](mailto:will.hancock007@gmail.com)

## Setup and Development
### System Requirements
Install the following;

* Environment: [Node.js](http://nodejs.org/)


### Run instructions
In your terminal;

* Checkout the repo ```git clone https://bitbucket.org/will-hancock/e2e-selenium-jasmine``` to your desired directory.
* Navigate to inside the repo in your terminal
* Install the npm modules ```npm install```
* Run; ```jasmine-node spec/``` 

### Configuration
See ```./config/config.json``` for configurable options.

e.g.

* Browser (chrome|firefox|ie)
* Test timeout limit
 